<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>

<head>
<meta charset="utf-8">
<title>ホーム</title>
</head>

<body>
	<div class="header">
		<h1>e-learning</h1>
	</div>

	<c:url value="/showMessage.html" var="messageUrl" />
	<a href="${messageUrl}">テスト開始</a>

</body>

</html>
