<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>回答</title>
</head>
<body>
	<div class="questions">
		<c:forEach items="${questions}" var="question">
			<div class="question">
				問題①<span class="text"> <c:out value="${question.text}" /></span> <br />
			</div>
			<div class="selects">
				<c:forEach items="${selects}" var="select">
						①<span class="select1"> <c:out value="${select.text}" /></span><br />
						②<span class="select2"> <c:out value="${select.text}" /></span><br />
						③<span class="select3"> <c:out value="${select.text}" /></span><br />
						④<span class="select4"> <c:out value="${select.text}" /></span><br />
				</c:forEach>
			</div>
		</c:forEach>
	</div>
</body>
</html>